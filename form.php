<?php
$genders = array(0 => "Nam", 1 => "Nữ");
$faculties = array(
	"MAT" => "Khoa học máy tính",
	"KDL" => "Khoa học vật liệu"
);
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
	<link href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css' rel='stylesheet'>
    <script src= "https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" ></script>
    <title>Đăng ký tân sinh viên</title>
</head>
<body>
    <form method="POST" action="" enctype="multipart/form-data">
		<div class="error-message">
			<?php
				if ($_SERVER["REQUEST_METHOD"] == "POST") {
					$flag = true;
					$flag2 = false;
					if (!isset($_POST["username"]) || empty($_POST["username"])) {
						echo "<p style=\"color: red\">Hãy nhập tên</p>";
						$flag = false;
					}
					if (!isset($_POST["gender"])) {
						echo "<p style=\"color: red\">Hãy chọn giới tính</p>";
						$flag = false;
					}
					if (!isset($_POST["faculty"]) || empty($_POST["faculty"])) {
						echo "<p style=\"color: red\">Hãy chọn phân khoa</p>";
						$flag = false;
					}
					if (!isset($_POST["dob"]) || empty($_POST["dob"])) {
						echo "<p style=\"color: red\">Hãy nhập ngay sinh</p>";
						$flag = false;
					}
					else {
						if (preg_match("/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/", $_POST["dob"]) === 0) {
							echo "<p style=\"color: red\">Hãy nhập ngay sinh đúng định dạng</p>";
							$flag = false;
						}
					}
					if(!empty($_FILES["image"]["name"])) {
						$image_file_extensions = array("image/jpeg", "image/png");
						$extension = $_FILES["image"]["type"];
						foreach ($image_file_extensions as $ext) {
							if (strcmp($extension, $ext) == 0) {
								$flag2 = true;
								break;
							}
						}
						if ($flag2 == false) {
							echo "<p style=\"color: red\">Hãy chọn file ảnh, định dạng: JPEG, PNG</p>";
							$flag = false;
						}
					}
					if ($flag) {
						$student_list = fopen("dssv.csv", "a");
						$index_of_student = count(file("dssv.csv"))+1;
						$new_student_info = "\n" . $index_of_student . "," . $_POST["username"] . "," . $_POST["faculty"];
						fwrite($student_list, $new_student_info);
						fclose($student_list);
						$target_image = "";
						if ($flag2) {
							if (!file_exists('./upload')) {
								mkdir('./upload', 0777, true);
							}
							$info = pathinfo($_FILES["image"]["name"]);
							$new_name = $info["filename"] . "_" . date("YmdHis") . ".";
							$ext = $info["extension"];
							$target_image = "./upload/" . $new_name . $ext;
							move_uploaded_file($_FILES["image"]["tmp_name"], $target_image);
						}
						session_start();
						$_SESSION = $_POST;
						$_SESSION["image"] = $target_image;
						header('location: home.php');
					}
				}
			?>
		</div>
        <div class="input-field-div">
            <div class="label-div">
                <label for="name" class="required-input">Họ và tên</label>
            </div>
            <input class="text-input" type="text" id="username" name="username">
        </div>
        <div class="input-field-div">
            <div class="label-div">
                <label for="gender" class="required-input">Giới tính</label>
            </div>
			<div class="gender-div">
				<?php
					for ($i = 0; $i < count($genders); ++$i) {
						echo ("
							<div>
								<input type=\"radio\" name=\"gender\" value=\"$i\">
								<label>$genders[$i]</label>
							</div>
						");
					}
				?>
			</div>
        </div>
		<div class="input-field-div">
            <div class="label-div">
                <label for="faculty" class="required-input">Phân khoa</label>
            </div>
			<select name="faculty" id="faculty">
				<?php
					echo "<option></option>";
					foreach ($faculties as $faculty) {
						echo "<option value=\"$faculty\">$faculty</option>";
					}
				?>
			</select>
        </div>
		<div class="input-field-div">
			<div class="label-div">
				<label for="dob" class="required-input">Ngày sinh</label>
			</div>
			<input class="my-date-picker" type="text" id="dob" name="dob" placeholder="dd/mm/yyyy">
		</div>
		<div class="input-field-div">
			<div class="label-div">
				<label for="address">Địa chỉ</label>
			</div>
			<input class="text-input" type="text" id="address" name="address">
		</div>
		<div class="input-field-div file-input">
			<div class="label-div">
				<label for="image">Hình ảnh</label>
			</div>
			<input type="file" id="image" name="image" onchange="console.info(this.files);">
		</div>
        <div class="submit-div">
            <input type="submit" value="Đăng ký"></input>
        </div>
    </form>
	
	<script type="text/javascript">
		$(document).ready(function() {
			$(function() {
				$("#dob").datepicker({
					dateFormat: 'dd/mm/yy'
				});
			});
		})
	</script>
</body>
</html>
